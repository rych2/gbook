package rych.gbook;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class DownLoader extends AsyncTask<String,Void,Void> {


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(String[] a_url) {

        try {
            URL url = new URL(a_url[0]);
            URLConnection conn=url.openConnection();
            conn.connect();
            String filename=a_url[0].substring(a_url[0].lastIndexOf('/'), a_url[0].indexOf('?'));
            //String filename="mav.pdf";
            String fullPath= Environment.getExternalStorageDirectory() + File.separator +filename;
            InputStream is=new BufferedInputStream(url.openStream(),8192);
            OutputStream os=new FileOutputStream(fullPath);
            byte data[]=new byte[1024];
            int count=0;
            while((count=is.read(data))!=-1){
                os.write(data,0,count);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }
} 
