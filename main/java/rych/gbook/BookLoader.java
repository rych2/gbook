package rych.gbook;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.annotation.NonNull;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonReader;
import android.util.Log;

import com.google.common.io.BaseEncoding;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

public class BookLoader extends AsyncTaskLoader {

    private final String mHttp="https://www.googleapis.com/books/v1/volumes?q=";
    private String mReq;
    private StringBuilder mFullReq;
    private Context mCtx;

    public BookLoader(Context context, String str){
        super(context);
        mCtx=context;
        mReq=str;
    }

    @Override
    public ArrayList<Book> loadInBackground() {
        ArrayList<Book> al=new ArrayList<Book>();
        URL url= null;
        //String req=new String();
        mFullReq=new StringBuilder(mHttp);
        mFullReq.append(mReq);
        mFullReq.append("&maxResults=40&projection=lite&filter=free-ebooks&key=AIzaSyAllD6YBKY-z6_hb6Z1zL5uTCMDzy7byKM");
        String packageName = mCtx.getPackageName();

        try {
            url = new URL(mFullReq.toString());
            HttpsURLConnection con=(HttpsURLConnection)url.openConnection();
            //con.setRequestMethod("GET");
            //con.setDoInput(true);
            InputStream is=con.getErrorStream();
            /*JsonReader reader=new JsonReader(new InputStreamReader(is,"UTF-8"));
            String name;
            StringBuilder auth=new StringBuilder();
            Book tmpBook;
            reader.beginObject();
            if(reader.hasNext()) {
                if(BLreader.findName(reader, "items")) {
                    Log.i("Ass", "itemsFound");
                    reader.beginArray();
                    while (reader.hasNext()) {
                        reader.beginObject();
                        BLreader.findName(reader, "volumeInfo");
                        reader.beginObject();
                        tmpBook = new Book();
                        while (reader.hasNext()) {
                            name = reader.nextName();
                            if (name.equals("title"))
                                tmpBook.mTitle = reader.nextString();
                            else if (name.equals("authors")) {
                                reader.beginArray();
                                for (auth.append(reader.nextString()); reader.hasNext(); auth.append(reader.nextString())) {
                                    //auth.append(reader.nextString());
                                    auth.append(", ");
                                }

                                reader.endArray();
                                tmpBook.mAuth = auth.toString();
                                auth.setLength(0);
                            } else if (name.equals("description"))
                                tmpBook.mDesc = reader.nextString();
                            else if (name.equals("imageLinks")) {
                                reader.beginObject();
                                while (reader.hasNext()) {
                                    name = reader.nextName();
                                    if (name.equals("thumbnail"))
                                        tmpBook.mPicAdress = reader.nextString();
                                    else
                                        reader.skipValue();
                                }
                                reader.endObject();
                            } else reader.skipValue();
                        }

                        reader.endObject();

                        if(BLreader.findName(reader, "accessInfo")){
                            Log.i("Ass", "accessFound");
                            reader.beginObject();
                            while (reader.hasNext()) {
                                name = reader.nextName();
                                if (name.equals("pdf")){
                                    reader.beginObject();
                                    while (reader.hasNext()) {
                                        name = reader.nextName();
                                        if (name.equals("downloadLink")) {
                                            tmpBook.mPdf = reader.nextString();
                                            //tmpBook.mPdf+="&key=AIzaSyAllD6YBKY-z6_hb6Z1zL5uTCMDzy7byKM";
                                        }
                                        else reader.skipValue();
                                    }
                                    reader.endObject();
                                }
                                else reader.skipValue();
                            }
                            reader.endObject();
                        }

                        while (reader.hasNext()) reader.skipValue();
                        reader.endObject();
                        al.add(tmpBook);
                    }
                    reader.endArray();
                }
            }
            reader.endObject();
            reader.close();*/

            Book testBook=new Book("l",null,null);
            Scanner s = new Scanner(is).useDelimiter("\\A");
            String result = s.hasNext() ? s.next() : "";
            testBook.mTitle=result;
            al.add(testBook);

            return al;
        }
        catch (MalformedURLException e) {
            al.add(0, new Book("MUE",null,null));
            return al;
        }

        catch (IOException e) {
            al.add(0, new Book("IO",null,null));
            return al;
        }
    }

    public void getReqest(String str){
        mReq=str;
    }

    //Auth

    private String getSHA1(String packageName){
        try {
            Signature[] signatures = mCtx.getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES).signatures;
            for (Signature signature: signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA-1");
                md.update(signature.toByteArray());
                return BaseEncoding.base16().encode(md.digest());
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
