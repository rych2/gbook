package rych.gbook;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;

public class Desc_act extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_desc_act);
        LinearLayout ll=findViewById(R.id.content_act2);
        Intent intent=getIntent();
        //Bundle bundle=intent.getBundleExtra("rych.gbook.Launch");
        //Book mBook=bundle.getSerializable("Book");
        final Book mBook=(Book)intent.getSerializableExtra("Book");
        TextView tw=new TextView(this);
        tw.setText(mBook.mPdf);
        ll.addView(tw);
        Button B2=(Button)findViewById(R.id.button2);
        final DownloadManager dm = (DownloadManager)this.getSystemService(DOWNLOAD_SERVICE);
        final Context ctx=this;
        B2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri=Uri.parse(mBook.mPdf);
                //Uri uri=Uri.parse("https://books.sonatype.com/mvnref-book/pdf/mvnref-pdf.pdf");
                DownloadManager.Request req=new DownloadManager.Request(uri);
                req.setDestinationInExternalPublicDir("/ass", "ass.pdf");
                dm.enqueue(req);
                req.setDestinationInExternalPublicDir("/ass", "ass.pdf");
                //new DownLoader().execute(mBook.mPdf);
                //new DownLoader().execute("http://maven.apache.org/maven-1.x/maven.pdf");
            }
        });
    }
}

