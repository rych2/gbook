package rych.gbook;


import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.SearchView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Book>> {

    private String mHttp="http";
    private ArrayList<Book> mBookList;
    private String tmp;
    private RecyclerView mRw;
    private GbAdapter mGbA;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mRw=findViewById(R.id.recycler_view);
        mRw.setLayoutManager(new LinearLayoutManager(this));

        //getSupportLoaderManager().initLoader(0,null,this).forceLoad();
        //mBookList=null;


        //FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        //fab.setOnClickListener(new View.OnClickListener() {
           // @Override
           // public void onClick(View view) {
            //    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            //            .setAction("Action", null).show();
           // }
      //  });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem Si=menu.findItem(R.id.app_bar_search);
        SearchView sw=(SearchView)Si.getActionView();
        //final TextView tw=findViewById(R.id.tw);
        //final TextView tw2=findViewById(R.id.textView);
        sw.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                tmp=s;
                getSupportLoaderManager().restartLoader(0,null,MainActivity.this).forceLoad();
                //tw.setText(mBookList.get(0).mTitle);
                //tw2.setText(mBookList.get(0).mAuth);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
            
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<ArrayList<Book>> onCreateLoader(int i, Bundle bundle) {
        BookLoader bl=new BookLoader(this,tmp);
        //bl.getReqest(tmp);
        return bl;
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Book>> loader, ArrayList<Book> s) {
       mGbA=new GbAdapter(s,this);
       mRw.setAdapter(mGbA);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Book>> loader) {

    }
}
